@extends('layouts.master')

@section('content')

<h1>All Notes</h1>

<ul class="list-group">

	@foreach($notes as $note)

		<li class="list-group-item"> {{ link_to("notes/$note->id", $note->title) }} </li>

	@endforeach

</ul>


@stop