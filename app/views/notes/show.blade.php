@extends('layouts.master')

@section('content')

	<h2>{{ $note->title }}</h2>

	<article>{{  $note->body }}</article>

	<p>{{ link_to('/', 'Go Back') }}</p>

@stop