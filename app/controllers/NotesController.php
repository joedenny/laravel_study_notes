<?php

class NotesController extends BaseController {

	public function index()
	{
		//fetch all notes

		$notes = Note::all();

		//load a view to display them
		return View::make('notes.index')->withNotes($notes);

	}


	public function show($id)
	{

		$note = Note::findOrFail($id);

		return View::make('notes.show')->withNote($note);

	}
}