<?php

Route::get('/', 'NotesController@index');

Route::get('notes/{id}', 'NotesController@show')->where('id', '\d+');